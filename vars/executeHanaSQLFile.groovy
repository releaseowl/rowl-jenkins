//@Grapes([
@GrabResolver(name='ro-snapshots', root='http://44.209.135.70:8081/nexus/repository/ro-snapshot')
@Grab(group='com.saparate', module='rate-saphana', version='0.0.1-SNAPSHOT')
@GrabConfig(systemClassLoader=true)
//        @Grab(group='com.sap.cloud.db.jdbc', module='ngdbc', version='2.17.7')
//        @Grab(group='org.apache.ivy', module='ivy', version='2.5.1'),
//        @Grab(group='org.apache.ant', module='ant', version='1.9.14'),
//]
//)

import com.ro.saphana.SQLExecuter

def call(String sqlFileName) {

    println("Test");
    HashMap<String, Object> additionalProps = new HashMap<>();

//        additionalProps.put("databaseName", "hotel");
    SQLExecuter se = new SQLExecuter("", "3ce884f6-2aab-4532-ab07-0ceb376ba3ea.hana.trial-us10.hanacloud.ondemand.com:443", "DBADMIN", "LpsJune\$2023", additionalProps, null);
    se.initConnection();

    File sqlFile = new File(sqlFileName);
    //log.info("Executing sql file: {}", sqlFile.getAbsolutePath());
        se.executeScript(sqlFile);

        se.execute("drop schema hotel_t1 cascade");
    println("Test-end");

}

//call("D:\\repos\\ro-apps\\rate-saphana\\src\\test\\resources\\sanitytest.sql")

