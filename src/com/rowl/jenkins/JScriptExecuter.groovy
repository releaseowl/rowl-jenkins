package com.rowl.jenkins
@GrabResolver(name='ro-snapshots', root='http://44.209.135.70:8081/nexus/repository/ro-snapshot')
@Grab(group='com.saparate', module='rate-saphana', version='0.0.1-SNAPSHOT')
//@GrabConfig(systemClassLoader=true)

import com.ro.saphana.SQLExecuter;

class JScriptExecuter implements Serializable {

    def steps;
    JScriptExecuter(steps) {
        this.steps = steps;
    }

    def executeSql(String filePath) {

        println("start of execution")
        HashMap<String, Object> additionalProps = new HashMap<>();

//        additionalProps.put("databaseName", "hotel");
        SQLExecuter se = new SQLExecuter("", "3ce884f6-2aab-4532-ab07-0ceb376ba3ea.hana.trial-us10.hanacloud.ondemand.com:443", "DBADMIN", "LpsJune\$2023", additionalProps, null);
        se.initConnection();

        File sqlFile = new File(filePath);
        //log.info("Executing sql file: {}", sqlFile.getAbsolutePath());
        se.executeScript(sqlFile);

        se.execute("drop schema hotel_t1 cascade");
        println("end of execution");

    }
}
